from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from PRConfig.TestFileDB import test_file_db
from PyConf.Algorithms import PrParameterisationData
from RecoConf.data_from_file import mc_unpackers
from PyConf.application import make_data_with_FetchDataFromFile

options.evt_max = -1
n_files_per_cat = 1
polarity = "MU"
options.ntuple_file = f"data/param_data_{polarity}.root"
input_files = (
    (
        test_file_db["upgrade_DC19_01_Bs2JPsiPhi_MD"].filenames[:n_files_per_cat]
        if polarity == "MD"
        else test_file_db["upgrade_DC19_01_Bs2JpsiPhiMU"].filenames[:n_files_per_cat]
    )
    + test_file_db[f"upgrade_DC19_01_Bs2PhiPhi{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Z2mumu{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Bd2Dstmumu{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Dst2D0pi{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Bd2Kstee{polarity}"].filenames[:n_files_per_cat]
    + test_file_db[f"upgrade_DC19_01_Dp2KSPip_{polarity}"].filenames[:n_files_per_cat]
)
options.input_files = input_files
options.input_type = "ROOT"
options.set_conds_from_testfiledb(f"upgrade_DC19_01_Dst2D0pi{polarity}")


def run_tracking_param_debug():
    param_data = PrParameterisationData(
        MCParticles=mc_unpackers()["MCParticles"],
        MCVPHits=mc_unpackers()["MCVPHits"],
        MCFTHits=mc_unpackers()["MCFTHits"],
        zRef=8520.0,
        TrackInfo=make_data_with_FetchDataFromFile("/Event/MC/TrackInfo"),
    )

    data = [param_data]

    return Reconstruction("run_tracking_debug", data)


run_reconstruction(options, run_tracking_param_debug)
