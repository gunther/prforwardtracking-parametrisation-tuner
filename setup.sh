#!/bin/bash

if [ ! -f "env/miniconda.sh" ]; then
    wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O env/miniconda.sh
fi

if [ ! -d "env/tuner_env" ]; then
  bash env/miniconda.sh -b -p env/tuner_env &&
  (
      source env/tuner_env/bin/activate
      conda install -y -c conda-forge mamba
      conda env create -f env/environment.yaml -n tuner
      conda activate tuner
      conda env config vars set PATH="/cvmfs/sft.cern.ch/lcg/external/texlive/latest/bin/x86_64-linux:${PATH}"
  )
fi
