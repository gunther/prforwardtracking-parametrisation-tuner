// param[0] + param[1]*ty^2 + param[2]*tx^2 + param[3]*tx tx_ref +
// param[4]*tx_ref^2 + param[5]*ty^4 + param[6]*ty^2 tx^2 + param[7]*ty^2 tx
// tx_ref + param[8]*ty^2 tx_ref^2 + param[9]*tx^4 + param[10]*tx^3 tx_ref +
// param[11]*tx_ref^4
static constexpr std::array<float, 12> fieldIntegralParamsRef{
    -1.2094486121528516f,  -2.7897043324822492f, -0.35976930628193077f,
    -0.47138558705675454f, -0.5600847231491961f, 14.009315350693472f,
    -16.162818973243674f,  -8.807994419844437f,  -0.8753190393972976f,
    2.98254201374128f,     0.9625408279466898f,  0.10200564097830103f};
