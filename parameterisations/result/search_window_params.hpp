// param[0]*inv_p_gev + param[1]*ty^2 inv_p_gev + param[2]*tx^2 inv_p_gev +
// param[3]*tx inv_p_gev pol_qop_gev + param[4]*inv_p_gev^3 + param[5]*tx^3
// pol_qop_gev + param[6]*tx^2 inv_p_gev^2 + param[7]*tx inv_p_gev^2 pol_qop_gev
// + param[8]*inv_p_gev^4 + param[9]*ty^2 tx^2 inv_p_gev + param[10]*ty^2 tx
// inv_p_gev pol_qop_gev + param[11]*ty^2 inv_p_gev^3 + param[12]*tx^4 inv_p_gev
static constexpr std::array<float, 13> momentumWindowParamsRef{
    4018.896625676043f,   6724.789549369031f,  3970.9093976497766f,
    -4363.5807241252905f, 1421.1056758688073f, 4934.07761471779f,
    6985.252911263751f,   -5538.28013195104f,  1642.8616070452542f,
    106068.96918885755f,  -94446.81037767915f, 26489.793756692892f,
    -23936.54391006025f};
