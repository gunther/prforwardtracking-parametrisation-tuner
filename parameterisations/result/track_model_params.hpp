// param[0]*dSlope_fringe + param[1]*tx dSlope_fringe + param[2]*ty
// dSlope_fringe + param[3]*tx^2 dSlope_fringe + param[4]*tx ty dSlope_fringe +
// param[5]*ty^2 dSlope_fringe
static constexpr std::array<float, 6> cxParams{
    2.335283084724005e-05f, -5.394341220986507e-08f, -1.1353152524130453e-06f,
    9.213281616649267e-06f, -6.76457896718169e-07f,  -0.0003740758569392804f};
// param[0]*dSlope_fringe + param[1]*tx dSlope_fringe + param[2]*ty
// dSlope_fringe + param[3]*tx^2 dSlope_fringe + param[4]*tx ty dSlope_fringe +
// param[5]*ty^2 dSlope_fringe
static constexpr std::array<float, 6> dxParams{
    -7.057523874477465e-09f, 1.0524178059699073e-11f, 6.46124765440666e-10f,
    2.595690034874298e-09f,  8.044356540608104e-11f,  9.933758467661586e-08f};
// param[0]*dSlope_fringe + param[1]*ty dSlope_fringe_abs + param[2]*ty tx
// dSlope_fringe + param[3]*ty^3 dSlope_fringe_abs + param[4]*ty tx^2
// dSlope_fringe_abs + param[5]*ty^3 tx dSlope_fringe + param[6]*ty tx^3
// dSlope_fringe + param[7]*ty^3 tx^2 dSlope_fringe_abs
static constexpr std::array<std::array<float, 8>, 6> yCorrParamsLayers{
    {{1.9141402652138315f, 154.61935746400832f, 3719.298754021463f,
      -6981.575944838166f, -67.7612042340458f, 41484.88865215446f,
      30544.717526101966f, 211219.00520598015f},
     {1.9802106454737567f, 146.34197177414035f, 3766.9995843145575f,
      -7381.001822418669f, 18.407833054380728f, 42635.398541425144f,
      31434.95400997568f, 218404.36150766257f},
     {2.6036680178541256f, 53.231282135657125f, 4236.335446831202f,
      -10844.798302911375f, 986.1498917330866f, 52670.269097485856f,
      39380.4857744525f, 281250.90766092145f},
     {2.6802443731107797f, 40.75834605688442f, 4296.645356936966f,
      -11234.776424245354f, 1115.363228090216f, 53813.817216417505f,
      40299.07624778942f, 288431.507847565f},
     {3.3827128857688793f, -76.61325300322648f, 4875.424130053332f,
      -14585.199358667853f, 2322.162251501158f, 63618.048819648175f,
      48278.83901554796f, 350657.56046107266f},
     {3.4657288815375846f, -90.58976402034898f, 4946.538479838353f,
      -14962.319670402725f, 2464.758450826609f, 64707.51942328425f,
      49179.43246319585f, 357681.17176708044f}}};
// param[0]*dSlope_fringe + param[1]*ty dSlope_fringe_abs + param[2]*ty tx
// dSlope_fringe + param[3]*ty^3 dSlope_fringe_abs + param[4]*ty tx^2
// dSlope_fringe_abs + param[5]*ty^3 tx dSlope_fringe + param[6]*ty tx^3
// dSlope_fringe + param[7]*ty^3 tx^2 dSlope_fringe_abs
static constexpr std::array<float, 8> yCorrParamsRef{
    2.5415524238347658f, 63.25841388467006f, 4187.534822693825f,
    -10520.25391602297f, 881.6859925052617f, 51730.04107647908f,
    38622.50428524951f,  275325.5721020971f};
// param[0]*ty tx dSlope_fringe + param[1]*ty dSlope_fringe^2 + param[2]*ty^3
// dSlope_fringe_abs + param[3]*ty tx^2 dSlope_fringe_abs + param[4]*ty tx^3
// dSlope_fringe + param[5]*ty^3 tx^2 dSlope_fringe_abs
static constexpr std::array<float, 6> tyCorrParamsRef{
    0.9346197967408639f, -0.4658007458482092f, -4.119808929050499f,
    2.9514781492224613f, 12.5961355543964f,    39.98472114588754f};
// param[0]*ty dSlope_fringe_abs + param[1]*ty tx dSlope_fringe + param[2]*ty
// dSlope_fringe^2 + param[3]*ty^3 dSlope_fringe_abs + param[4]*ty tx^2
// dSlope_fringe_abs
static constexpr std::array<float, 5> cyParams{
    -1.2034772990836242e-05f, 8.344645618037317e-05f, -3.924972865228243e-05f,
    0.00024639290417116324f, 0.0001867723161873795f};
// param[0]*ty dSlope_xEndT^2 + param[1]*ty dSlope_yEndT^2
static constexpr std::array<float, 2> bendYParams{-1974.6355416889746f,
                                                  -35933.837494833504f};
