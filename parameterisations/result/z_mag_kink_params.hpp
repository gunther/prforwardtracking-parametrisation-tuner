// param[0] + param[1]*tx^2 + param[2]*tx dSlope_fringe + param[3]*ty^2 +
// param[4]*dSlope_fringe^2
static constexpr std::array<float, 5> zMagnetParamsRef{
    5205.144186525624f, -320.7206595710594f, 702.1384894815535f,
    -316.36350963107543f, 441.59909857558097f};
// param[0] + param[1]*dSlope_xEndT_abs + param[2]*x_EndT_abs + param[3]*tx^2 +
// param[4]*dSlope_xEndT^2
static constexpr std::array<float, 5> zMagnetParamsEndT{
    5286.687877988849f, -3.259689996453795f, 0.015615778872337033f,
    -1377.3175211789967f, 282.9821232487341f};
