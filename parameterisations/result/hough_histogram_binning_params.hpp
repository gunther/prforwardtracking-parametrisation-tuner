// p[0] + p[1] * x / (1 + p[2] * abs(x)) for nBins = 1152
constexpr auto p =
    std::array{576.9713937732083f, 0.5780266207743059f, 0.0006728484590464921f};
