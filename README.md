# Parameterisation Tuner

This project provides utils for producing magic parameters used by the pattern recognition algorithms in the Rec project. Typical parameters are coefficients for extrapolation polynomials and weights for TMVA methods.

## Setup
There's a bash script for setting up the necessary (python) environment. Simply do:
```
chmod +x setup.sh
./setup.sh
```
This will install dependencies like ROOT and Jupyter. To enter the environment do:
```
source env/tuner_env/bin/activate
conda activate tuner
```
